Office.onReady((info) => {
  console.log("Office is ready");
  // Add an event listener for the button click after Office is ready
  document.getElementById("generate-response").addEventListener("click", waitForMailbox);
});

async function waitForMailbox() {
  if (!Office.context || !Office.context.mailbox) {
    console.log("Waiting for Office.context.mailbox to be available...");
    setTimeout(waitForMailbox, 100);
    return;
  }
  await generateResponse();
}

async function generateResponse() {
  try {
    console.log("Generate Response button clicked");

    const item = Office.context.mailbox.item;
    const conversation = await getConversation(item);
    const draftResponse = await generateDraftResponse(conversation);

    // Display the draft response
    console.log(draftResponse);
    document.getElementById("response").innerText = draftResponse;
  } catch (error) {
    console.error(error);
    document.getElementById("error-message").innerText = error.message;
    document.getElementById("error-message").style.display = "block";
  }
}

function getConversation(item) {
  return new Promise((resolve, reject) => {
    item.body.getAsync(Office.CoercionType.Text, (result) => {
      if (result.status === Office.AsyncResultStatus.Succeeded) {
        resolve(result.value);
      } else {
        reject(new Error("Failed to get email conversation"));
      }
    });
  });
}

async function generateDraftResponse(conversation) {
  const openaiApiKey = "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";
  const prompt = `Generate a draft email response for the following conversation:\n\n${conversation}`;

  const response = await fetch("https://api.openai.com/v2/engines/davinci-codex/completions", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${openaiApiKey}`
    },
    body: JSON.stringify({
      prompt: prompt,
      max_tokens: 150,
      n: 1,
      stop: null,
      temperature: 0.5,
    }),
  });

  if (!response.ok) {
    const errorData = await response.json();
    console.error("OpenAI API error:", errorData);
    throw new Error(`OpenAI API call failed with status: ${response.status}`);
  }

  const data = await response.json();
  return data.choices[0].text.trim();
}

// Check if Office is already initialized, and if so, add the event listener
if (Office.context && Office.context.mailbox) {
  document.getElementById("generate-response").addEventListener("click", generateResponse);
}